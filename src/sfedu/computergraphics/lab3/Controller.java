package sfedu.computergraphics.lab3;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.util.Pair;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Stack;

public class Controller implements Initializable {

    private final static int SCALE = 10;

    @FXML
    private ImageView canvas;

    @FXML
    private ToggleButton fillTool;

    @FXML
    private ColorPicker fillColor;

    @FXML
    private ToggleButton pencilTool;

    @FXML
    private ColorPicker pencilColor;

    @FXML
    private Button clearTool;

    @FXML
    private ToggleButton alternativeFillSwitcher;

    private WritableImage image;

    private PixelWriter writer;

    private PixelReader reader;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        image = new WritableImage((int) canvas.getFitWidth(), (int) canvas.getFitHeight());
        writer = image.getPixelWriter();
        reader = image.getPixelReader();
        canvas.setImage(image);

        pencilTool.setSelected(true);
        pencilColor.setValue(Color.BLACK);
        fillColor.setValue(Color.INDIGO);
        onClearAction();

        canvas.setOnMouseClicked(event -> onMouseClick((int) (event.getX() / SCALE), (int) (event.getY() / SCALE)));
        canvas.setOnMouseDragged(event -> onMouseClick((int) (event.getX() / SCALE), (int) (event.getY() / SCALE)));
        clearTool.setOnAction(event -> onClearAction());
    }

    /**
     * Mouse click processing by (X; Y) coordinates
     *
     * @param x X coordinate in image metric
     * @param y Y coordinate in image metric
     */
    private void onMouseClick(int x, int y) {
        if (pencilTool.isSelected()) {
            drawPoint(x, y, pencilColor.getValue());
        }
        if (fillTool.isSelected()) {
            if (alternativeFillSwitcher.isSelected()) {
                spreadFillOptimized(x, y, fillColor.getValue());
            } else {
                spreadFill(x, y, fillColor.getValue());
            }
        }
    }

    /**
     * Fill 8-connected area starting with (Xi; Yi) with specified color
     * until pencil color is met
     *
     * @param xi    X coordinate in image metric
     * @param yi    Y coordinate in image metric
     * @param color color to fill with
     */
    private void spreadFill(int xi, int yi, Color color) {
        int maxStackSize = 1;
        long start = System.currentTimeMillis();
        boolean used[][] = new boolean[(int) canvas.getFitWidth()][(int) canvas.getFitHeight()];
        Stack<Pair<Integer, Integer>> stack = new Stack<>();
        stack.push(new Pair<>(xi, yi));
        while (!stack.empty()) {
            try {
                int x = stack.peek().getKey();
                int y = stack.peek().getValue();
                stack.pop();
                if (!used[x][y]) {
                    used[x][y] = true;
                    if (!reader.getColor(x * SCALE, y * SCALE).equals(pencilColor.getValue())) {
                        drawPoint(x, y, color);
                        stack.push(new Pair<>(x - 1, y));
                        stack.push(new Pair<>(x + 1, y));
                        stack.push(new Pair<>(x - 1, y - 1));
                        stack.push(new Pair<>(x, y - 1));
                        stack.push(new Pair<>(x + 1, y - 1));
                        stack.push(new Pair<>(x - 1, y + 1));
                        stack.push(new Pair<>(x, y + 1));
                        stack.push(new Pair<>(x + 1, y + 1));
                    }
                }
            } catch (IndexOutOfBoundsException e) {
            }
            maxStackSize = Math.max(maxStackSize, stack.size());
        }
        System.out.println("Simple filling algorithm");
        System.out.println("Filled in " + (System.currentTimeMillis() - start) + " ms");
        System.out.println("Max stack size is " + maxStackSize);
    }

    final static int X_OFFSET[] = {-1, -1, -1, +0, +1, +1, +1, +0};
    final static int Y_OFFSET[] = {+1, +0, -1, -1, -1, +0, +1, +1};

    /**
     * Fill 8-connected area starting with (Xi; Yi) with specified color
     * until pencil color is met (optimized version)
     *
     * @param xi    X coordinate in image metric
     * @param yi    Y coordinate in image metric
     * @param color color to fill with
     */
    private void spreadFillOptimized(int xi, int yi, Color color) {
        int maxStackSize = 1;
        long start = System.currentTimeMillis();
        boolean used[][] = new boolean[(int) canvas.getFitWidth()][(int) canvas.getFitHeight()];
        Stack<Pair<Integer, Integer>> stack = new Stack<>();
        used[xi][yi] = true;
        if (!reader.getColor(xi * SCALE, yi * SCALE).equals(pencilColor.getValue())) {
			drawPoint(xi, yi, color);
		}
        stack.push(new Pair<>(xi, yi));
        while (!stack.empty()) {
            try {
                int x = stack.peek().getKey();
                int y = stack.peek().getValue();
                stack.pop();
                for (int i = 0; i < 8; i++) {
                    if (!used[x + X_OFFSET[i]][y + Y_OFFSET[i]]) {
                        used[x + X_OFFSET[i]][y + Y_OFFSET[i]] = true;
                        if (!reader.getColor((x + X_OFFSET[i]) * SCALE, (y + Y_OFFSET[i]) * SCALE)
                                .equals(pencilColor.getValue())) {
                            drawPoint(x + X_OFFSET[i], y + Y_OFFSET[i], color);
                            stack.push(new Pair<>(x + X_OFFSET[i], y + Y_OFFSET[i]));
                        }
                    }
                }
            } catch (IndexOutOfBoundsException e) {
            }
            maxStackSize = Math.max(maxStackSize, stack.size());
        }
        System.out.println("Optimized filling algorithm");
        System.out.println("Filled in " + (System.currentTimeMillis() - start) + " ms");
        System.out.println("Max stack size is " + maxStackSize);
    }

    /**
     * Draw point (Xi; Yi) in image coordinates with specified color
     *
     * @param x     X coordinate in image metric
     * @param y     Y coordinate in image metric
     * @param color color to draw
     */
    private void drawPoint(int x, int y, Color color) {
        for (int i = 0; i < SCALE; i++) {
            for (int j = 0; j < SCALE; j++) {
                try {
                    writer.setColor(x * SCALE + i, y * SCALE + j, color);
                } catch (IndexOutOfBoundsException e) {
                }
            }
        }
    }

    /**
     * Clear whole image with white color
     */
    private void onClearAction() {
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                writer.setColor(i, j, Color.WHITE);
            }
        }
    }
}
